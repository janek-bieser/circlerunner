﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class CoinCollector : MonoBehaviour {

	public AudioClip coinSound;
	private bool isCollected = false;

	void OnTriggerEnter2D(Collider2D other) {
		if (isCollected) return;

		LevelStats.CollectedCoins++;
		isCollected = true;

		AudioSource source = GetComponent<AudioSource>();
		if (source != null) {
			source.PlayOneShot(coinSound);
		}

		// Remove rendering and physics components to disable
		// all interaction and visuals
		Destroy(GetComponent<SpriteRenderer>());
		Destroy(GetComponent<BoxCollider2D>());

		// Delay destory to finish playing the sound first
		Destroy(gameObject, 0.8f);
	}

}
