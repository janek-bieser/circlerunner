﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class GoalDetector : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {
		if (GameState.Paused) return;

		if (LevelStats.AllCoinsCollected()) {
			LevelStats.EndTime = Time.time;
			GameState.Paused = true;
			GameState.LevelComplete = true;
			Application.LoadLevelAdditiveAsync(1);
		}
	}

}
