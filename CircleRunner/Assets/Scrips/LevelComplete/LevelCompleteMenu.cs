﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelCompleteMenu : MonoBehaviour {

	public Text timeLabel;

	// Use this for initialization
	void Start () {
		float time = LevelStats.EndTime - LevelStats.StartTime;
		float currentScore = Score.GetScoreForLevel(LevelStats.CurrentLevel);
		string highscoreText = "";

		if (currentScore <= 0 || time < currentScore) {
			highscoreText = "\n(New Highscore)";
			Score.SetScoreForLevel(LevelStats.CurrentLevel, time);
		}

		float min = Mathf.Floor(time / 60.0f);
		float sec = Mathf.Floor(time % 60.0f);
		float frac = Mathf.Floor((time * 100) % 100);

		timeLabel.text = string.Format("{0:00}:{1:00}:{2:00}" + highscoreText, min, sec, frac);


	}

	public void RestartLevel() {
		Application.LoadLevel(LevelStats.CurrentLevelID);
	}

	public void NextLevel() {
		Application.LoadLevel(LevelStats.GetNextLevel());
	}
	
}
