﻿using UnityEngine;
using UnityEngine.UI;

public class LevelDieMenu : MonoBehaviour {

	public void RestartLevel() {
		Application.LoadLevel(LevelStats.CurrentLevelID);
	}

	public void BackToMainMenu() {
		Application.LoadLevel(0);
	}

}
