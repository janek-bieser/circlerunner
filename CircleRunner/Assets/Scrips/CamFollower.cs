﻿using UnityEngine;
using System.Collections;

public class CamFollower : MonoBehaviour {

	public GameObject objectToFolow;

	private Vector3 diff = Vector3.zero;

	// Use this for initialization
	void Start () {
		diff = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameState.LevelComplete) return;

		diff.x = objectToFolow.transform.position.x;
		diff.y = objectToFolow.transform.position.y;
		transform.position = diff;
	}
}
