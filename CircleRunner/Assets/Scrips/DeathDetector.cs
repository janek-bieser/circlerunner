﻿using UnityEngine;
using System.Collections;

public class DeathDetector : MonoBehaviour {

	public Transform minTransform;

	// Update is called once per frame
	void Update () {
		if (GameState.Paused) return;

		if (transform.position.y < minTransform.position.y) {
			GameState.Paused = true;
			GameState.LevelComplete = true;
			Application.LoadLevelAdditive(2);
		}
	}
}
