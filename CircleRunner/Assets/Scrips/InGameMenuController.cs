﻿using UnityEngine;
using UnityEngine.UI;

public class InGameMenuController : MonoBehaviour {

	public GameObject menuContainer;
	private bool showingMenu = false;

	void Start () {
		menuContainer.SetActive(showingMenu);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Cancel")) {
			showingMenu = !showingMenu;
			setMenuState();
		}
	}

	private void setMenuState() {
		GameState.Paused = showingMenu;
		menuContainer.SetActive(showingMenu);
		Time.timeScale = (GameState.Paused) ? 0 : 1;
	}

	public void ResumeLevel() {
		showingMenu = false;
		setMenuState();
	}

	public void RestartLevel() {
		showingMenu = false;
		setMenuState();
		Application.LoadLevel(LevelStats.CurrentLevelID);
	}

	public void BackToMainMenu() {
		Application.LoadLevel(0);
	}
}
