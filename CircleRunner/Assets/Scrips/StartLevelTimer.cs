﻿using UnityEngine;
using UnityEngine.UI;

public class StartLevelTimer : MonoBehaviour {

	public Text timerText;
	public float countdown = 3;

	private float currentTime = 0;

	// Use this for initialization
	void Start () {
		GameState.Paused = true;
		currentTime = countdown;
	}
	
	// Update is called once per frame
	void Update () {
		currentTime -= Time.deltaTime;
		timerText.text = Mathf.Ceil(currentTime).ToString();;

		if (currentTime <= 0) {
			// Start Game
			Destroy(gameObject);
			LevelStats.ResetStartTime();
			GameState.Paused = false;
		}
	}
}
