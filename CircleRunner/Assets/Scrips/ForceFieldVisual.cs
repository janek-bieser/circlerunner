﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(ForceField))]
public class ForceFieldVisual : MonoBehaviour {

	public Animator[] visuals;

	private ForceField field;

	// Use this for initialization
	void Start () {
		field = GetComponent<ForceField>();
	}
	
	// Update is called once per frame
	void Update () {
		float p = field.GetCurrentPercent();

		foreach (Animator a in visuals) {
			a.SetFloat("percent", p);
		}
	}
}
