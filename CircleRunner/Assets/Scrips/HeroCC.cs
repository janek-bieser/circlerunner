﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class HeroCC : MonoBehaviour {

	Rigidbody2D body;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GameState.Paused) return;

		float speed = -Input.GetAxisRaw("Horizontal") * 2.5f;
		body.AddTorque(speed);
	}
}
