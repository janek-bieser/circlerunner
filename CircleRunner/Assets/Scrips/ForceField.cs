﻿using UnityEngine;
using System.Collections;

public class ForceField : MonoBehaviour {

	public float triggerInterval = 4;
	public float force = 10;

	private GameObject currentTarget;
	private float currentTime;
	private float percent = 0;

	// Use this for initialization
	void Start () {
		currentTime = triggerInterval;
	}
	
	// Update is called once per frame
	void Update () {
		if (GameState.Paused) return;

		currentTime -= Time.deltaTime;

		percent = 1 - (currentTime / triggerInterval);

		if (currentTime <= 0) {
			percent = 0;

			if (currentTarget != null) {
				Rigidbody2D body = currentTarget.GetComponent<Rigidbody2D>();
				float velocity = Mathf.Sqrt(-2.0f * Physics2D.gravity.y * force);
				body.AddForce(new Vector2(0, body.mass * velocity), ForceMode2D.Impulse);
			}

			currentTime = triggerInterval;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		currentTarget = other.gameObject;
	}

	void OnTriggerExit2D(Collider2D other) {
		if (currentTarget == other.gameObject) {
			currentTarget = null;
		}
	}

	public float GetCurrentPercent() {
		return percent;
	}
}
