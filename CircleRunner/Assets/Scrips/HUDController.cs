﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

	public Text coinCount;
	public Text timeDisplay;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		coinCount.text = LevelStats.CollectedCoins + " / " + LevelStats.TotalNumberOfCoins;

		if (GameState.Paused) return;

		float time = Time.time - LevelStats.StartTime;

		float min = Mathf.Floor(time / 60.0f);
		float sec = Mathf.Floor(time % 60.0f);
		float frac = Mathf.Floor((time * 100) % 100);

		timeDisplay.text = string.Format("{0:00}:{1:00}:{2:00}", min, sec, frac);
	}
}
