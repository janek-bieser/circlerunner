﻿using UnityEngine;
using System.Collections;

public class LevelInitializer : MonoBehaviour {

	public Transform coinsContainer;

	void Start () {
		Score.InitializeHighscores();
		LevelStats.ResetLevel(coinsContainer.childCount);
		GameState.Paused = true;
		Time.timeScale = 1;
	}
}
