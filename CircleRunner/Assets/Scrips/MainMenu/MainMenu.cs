﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public GameObject mainMenu;
	public GameObject levelMenu;
	public GameObject highscoreMenu;

	public Text highscoreText;

	public void Start() {
		LevelStats.SetLevel(0);
		Score.InitializeHighscores();
		ShowMainMenu();
	}

	public void StartGame() {
		Application.LoadLevel(LevelStats.CurrentLevelID);
	}

	public void StartLevel(int level) {
		LevelStats.SetLevel(level);
		Application.LoadLevel(LevelStats.CurrentLevelID);
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void ShowMainMenu() {
		mainMenu.SetActive(true);
		levelMenu.SetActive(false);
		highscoreMenu.SetActive(false);
	}

	public void ShowLevelSelection() {
		mainMenu.SetActive(false);
		levelMenu.SetActive(true);
		highscoreMenu.SetActive(false);
	}

	public void ShowHighscores() {
		mainMenu.SetActive(false);
		levelMenu.SetActive(false);
		highscoreMenu.SetActive(true);
		SetHighscoreText();
	}

	private void SetHighscoreText() {
		string scoreString = "";

		for (int i = 0; i < LevelStats.LevelCount; i++) {
			float time = Score.GetScoreForLevel(i);

			if (time <= 0) {
				scoreString += "Level " + (i + 1) + " --         /\n";
			} else {
				float min = Mathf.Floor(time / 60.0f);
				float sec = Mathf.Floor(time % 60.0f);
				float frac = Mathf.Floor((time * 100) % 100);
				scoreString += string.Format("Level {0} -- {1:00}:{2:00}:{3:00}\n", i + 1, min, sec, frac);
			}
		}

		highscoreText.text = scoreString;
	}
}
