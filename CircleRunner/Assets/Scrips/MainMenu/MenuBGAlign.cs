﻿using UnityEngine;
using System.Collections;

public class MenuBGAlign : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SpriteRenderer sp = GetComponent<SpriteRenderer>();

		Vector3 scale = new Vector3(1, 1, 1);

		float width = sp.sprite.bounds.size.x;
		float height = sp.sprite.bounds.size.y;

		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		if (worldScreenWidth / width > worldScreenHeight / height) {
			scale.x = worldScreenWidth / width;
			scale.y = scale.x;
		} else {
			scale.y = worldScreenHeight / height;
			scale.x = scale.y;
		}

		transform.localScale = scale;
	}
}
