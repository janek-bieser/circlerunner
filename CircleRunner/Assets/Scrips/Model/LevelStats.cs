﻿using UnityEngine;
using System.Collections;

public class LevelStats {

	private static int[] levelIDs = {
		3, 4, 5, 6
	};

	private static int currentLevel = 0;

	public static int LevelCount { get { return levelIDs.Length; } }

	public static int CurrentLevelID { get { return levelIDs[currentLevel]; } }
	public static int CurrentLevel { get { return currentLevel; } }

	private static int coinsToCollect = 0;
	private static float startTime = 0;

	public static int CollectedCoins { get; set; }
	public static int TotalNumberOfCoins { get { return coinsToCollect; } }
	public static float StartTime { get { return startTime; } }
	public static float EndTime { get; set; }

	public static void ResetLevel(int coinCount) {
		LevelStats.coinsToCollect = coinCount;
		CollectedCoins = 0;
		startTime = Time.time;
		EndTime = -1;
		GameState.LevelComplete = false;
	}

	public static void ResetStartTime() {
		startTime = Time.time;
	}

	public static int GetNextLevel() {
		currentLevel++;
		if (currentLevel >= levelIDs.Length) {
			currentLevel = 0;
		}
		return CurrentLevelID;
	}

	public static bool AllCoinsCollected() {
		return CollectedCoins >= coinsToCollect;
	}

	public static void SetLevel(int value) {
		currentLevel = value;
		if (currentLevel >= levelIDs.Length) {
			currentLevel = 0;
		}
	}

}
