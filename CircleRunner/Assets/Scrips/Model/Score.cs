﻿using UnityEngine;
using System.Collections;

public class Score {

	private static float[] highscores = new float[LevelStats.LevelCount];

	private static bool initialized = false;

	public static void InitializeHighscores() {
		if (initialized) return;
		initialized = true;

		for (int i = 0; i < LevelStats.LevelCount; i++) {
			highscores[i] = PlayerPrefs.GetFloat(i.ToString());
		}
	}

	public static float GetScoreForLevel(int level) {
		if (level < 0 || level >= LevelStats.LevelCount) return -1;
		return highscores[level];
	}

	public static void SetScoreForLevel(int level, float score) {
		if (level < 0 || level >= LevelStats.LevelCount) return;
		highscores[level] = score;
		PlayerPrefs.SetFloat(level.ToString(), score);
	}

}
