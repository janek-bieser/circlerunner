﻿using UnityEngine;
using System.Collections;

public class GameState {

	public static bool Paused { get; set; }
	public static bool LevelComplete { get; set; }

}
