﻿using UnityEngine;
using System.Collections;

public class BGAlign : MonoBehaviour {


	private float screenHeight;

	// Use this for initialization
	void Start () {
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

		transform.localScale = new Vector3(worldScreenWidth * 1.5f, worldScreenHeight * 1.5f, 1);

		Vector3 pos = transform.position;
		pos.y = -worldScreenHeight * 1.5f / 2f;
		transform.position = pos;

		screenHeight = worldScreenHeight;
	}

	void Update() {
		Vector3 camPos = Camera.main.transform.position;
		Vector3 pos = transform.position;
		pos.x = camPos.x;
		pos.y = -screenHeight * 1.5f / 2f;

		if (camPos.y < -screenHeight / 2) {
			pos.y = camPos.y;
		}

		transform.position = pos;
	}

}
